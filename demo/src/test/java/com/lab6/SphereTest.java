package com.lab6;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;

/** 
 * test class for Sphere class
 * tests that get Volume class works, 
 * tests that get surface area class works, 
 * tests that get Radius class works.
 * @author Charles-Alexandre Bouchard 2135704
 * @version 9/16/2022
*/

public class SphereTest 
{

    public double delta = 1e-2;
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testSphere_Constructor(){
        // IllegalArgumentException exception = assertThrows(
        //     IllegalArgumentException.class,
        //     ()-> {Sphere sphere = new Sphere(5.22);}); //Test for both legal and illegal value of radius

        // String expectedMessage = "The radius can not be negative or zero";
        // String actualMessage = exception.getMessage(); 

        // assertTrue(actualMessage.contains(expectedMessage));
        assertDoesNotThrow(()->{Sphere sphere = new Sphere(5);});
    }

    @Test
    public void testSphere_GetVolume()
    {
        Sphere sphere = new Sphere(5);
        double expected_volume = 523.598775598299;
        //assertEquals((4.0/3.0)*Math.PI * Math.pow(sphere.getRadius(),3), sphere.getVolume(), delta);
        assertEquals(expected_volume, sphere.getVolume(),delta);
    }
    @Test
    public void testSphere_getSurfaceArea()
    {
        Sphere sphere = new Sphere(5);
        double expected_SA = 314.159265358979;
        assertEquals(expected_SA, sphere.getSurfaceArea(),delta);
    }
    @Test
    public void testSphere_getRadius()
    {
        Sphere sphere = new Sphere(5);
        assertEquals(5, sphere.getRadius(), delta);
    }

}
