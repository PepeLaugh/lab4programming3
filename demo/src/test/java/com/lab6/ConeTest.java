/** 
 * test class for Cone class
 * tests get Volume class works, 
 * tests get Surface area class works, 
 * tests get Radius class works.
 * tests get Height class works.
 * @author Ojini Barsoumian 1840580
 * @version 9/16/2022
*/
package com.lab6;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class ConeTest {

    static double delta = 1e-2;

    @Test
    public void constructorTest(){
        // IllegalArgumentException exception = assertThrows(
        //     IllegalArgumentException.class,
        //     ()->{Cone cone = new Cone(2,3);});
        // String expectedMessage = "Radius and height need to be positive";
        // String actualMessage = exception.getMessage();

        // assertTrue(actualMessage.contains(expectedMessage));

        assertDoesNotThrow(()->{Cone cone = new Cone(2,3);});
        

    }

    @Test
    public void getVolumeTest(){
        Cone cone = new Cone(2,3);
        double expected_volume = 12.571428571428;
        assertEquals(expected_volume,cone.getVolume(),delta);
    }

    @Test
    public void getSurfaceAreaTest(){
        Cone cone = new Cone(2,3);
        double expected_SA = 35.22071741263713;
        //assertEquals((Math.PI * cone.getRadius() * (cone.getRadius() + Math.sqrt(Math.pow(cone.getHeight(), 2)+Math.pow(cone.getRadius(), 2)))),cone.getSurfaceArea());
        assertEquals(expected_SA,cone.getSurfaceArea(),delta);
        
    }
    @Test
    public void testCone_getRadius()
    {
        Cone cone = new Cone(2,3);
        assertEquals(2, cone.getRadius(), delta);
    }
    @Test
    public void testCone_getHeight()
    {
        Cone cone = new Cone(2,3);
        assertEquals(3, cone.getHeight(), delta);
    }

    
}
