package com.lab6;

/** 
 * Cone class that calculates volume of a cone, 
 * calculates the surface area of a cone, 
 * @author Charles-Alexandre Bouchard 2135704
 * @version 9/16/2022
*/

public class Cone 

{   
    private double r;
    private double h;
    
    public Cone(double r, double h)throws IllegalArgumentException{
        if(r<=0){
            throw new IllegalArgumentException("Radius needs to be positive");
        }
        if(h<=0){
            throw new IllegalArgumentException("Height needs to be positive");
        }
        this.r = r;
        this.h = h;
    }
    public double getRadius(){
        return this.r;
        //return 0;
    }
    public double getHeight(){
        return this.h;
        //return 0;
    }
    public double getVolume(){
        return (Math.PI * Math.pow(this.r, 2)*this.h/3.0);
        //return 0;

    }
    public double getSurfaceArea(){
        return Math.PI * this.r * (this.r + Math.sqrt(Math.pow(this.h, 2)+Math.pow(this.r, 2)));
        //return 0;

    }
}



