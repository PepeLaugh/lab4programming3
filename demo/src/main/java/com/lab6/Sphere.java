/** 
 * Sphere class that calculates the volume of a Sphere, 
 * calculates the surface area of a Sphere, 
 * @author Ojini Barsoumian 1840580
 * @version 9/16/2022
*/

package com.lab6;

public class Sphere {
    private double r;
    
    public Sphere(double r) throws IllegalArgumentException{
        this.r=r;
        if(r <= 0){
            throw new IllegalArgumentException("The radius can not be negative or zero");
        }
    }

    public double getRadius() {
        return this.r;
        //return 0; //dummy return for unit testing
    }

    public double getVolume() {
        return (4.0/3.0) * (Math.PI * (Math.pow(r, 3)));
        //return 0; //dummy return for unit testing
    }
    
    public double getSurfaceArea() {
        return 4.0 * Math.PI * (Math.pow(this.r, 2));
        //return 0; //dummy return for unit testing
    }

}
